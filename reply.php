<?php
include 'dbcon.php';
include 'tripcode.php';
// check form
if(!isset($_POST['content']) || trim($_POST['content']) == ''){
	die("You must write something");
}

// detect anon
if(!isset($_POST['name']) || trim($_POST['name']) == ''){
	$name = 'Anonymous';
} else{
	$name = $_POST['name'];
}

if(!isset($_POST['tripcode']) || trim($_POST['tripcode']) == ''){
	$tripcode = NULL;
} else{
	$tripcode = gentripcode($_POST['tripcode']);
}
// do query
$st = $db->prepare("insert into www.messageboard(is_op, reply_to, name, content, post_date, tripcode, user_ip) values(0, ?, ?, ?, current_timestamp(), ?, ?)");

if($st == false){
	echo $db->errno . ' ' . $db->error;
} else{
	$st->bind_param('sssss', htmlentities($_POST['thread_id']), htmlentities($name), nl2br(htmlentities($_POST['content'])), htmlentities($tripcode), $user_ip);
	$st->execute();
}


header('location: /thread.php/?thread=' . $_POST['thread_id']);
?>
