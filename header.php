<!DOCTYPE html>
<html>
<head>
<?php
if(!isset($_COOKIE['theme']) || trim($_COOKIE['theme']) == ''){
	$theme = 'blue';
} else{
	$theme = $_COOKIE['theme'];
}

echo '<link type="text/css" rel="stylesheet" href="/assets/' . $theme . '.css">';

?>
</head>
<body>
<nav>
	<div class="navleft">
	<a href="/index.php">Home</a> |
	<a href="/about.php">About</a> |
	<a href="/howto.php">How-To</a> 
	</div>
	<div class="navright">
	<form method="post" id="themechooser" action="/settheme.php">
	Themes: 
	<input type="submit" name="theme" value="blue">
	<input type="submit" name="theme" value="yotsuba">
	<input type="submit" name="theme" value="futaba">
	<input type="submit" name="theme" value="cute">
	<input type="submit" name="theme" value="hacker">
	</form>
	</div>
</nav> 
