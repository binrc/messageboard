<?php
include "header.php"
?>
<h1> Delete a post: </h1>
<p>go to <a href="/delete.php"> delete.php </a>, enter the post ID, a reason for deletion, and the deletion password</p>

<h1> Add a theme: </h1>
<p>Create a theme in <code>/assets</code>, then edit the portion of <code>header.php</code> that looks like this:
<pre><code>
&lt;div class="navright"&gt;
&lt;form method="post" id="themechooser" action="/settheme.php"&gt;
&lt;hemes:
&lt;input type="submit" name="theme" value="blue"&gt;
&lt;input type="submit" name="theme" value="yotsuba"&gt;
&lt;input type="submit" name="theme" value="futaba"&gt;
&lt;input type="submit" name="theme" value="cute"&gt;
&lt;input type="submit" name="theme" value="hacker"&gt;
&lt;/form&gt;
&lt;/div&gt;
</code>
</pre>

To add a theme, add an additional imput element where value = the name of your css file *without* the .css extension.
</p>
</body>
</html>
