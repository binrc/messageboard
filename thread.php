<?php
include "header.php";
?>
<div class="inputbox">
<p><b>Reply to thread</b></p>
<form action="/reply.php" method="post" id="newthread">
Name: <input type="text" name="name"><br>
Password: <input type="password" name="tripcode">
<?php
echo '<input readonly class="threadref" type="text" method="post" name="thread_id" value="' . $_GET['thread'] . '">';
?>
<input type="submit" value="Submit">
</form>
<textarea required spellcheck="true" rows="5" cols="50" name="content" form="newthread"></textarea>
</div>
<br>
<div class="threadbox">
<?php
include 'tripcolor.php';
include 'dbcon.php';
$st = $db->prepare("select * from www.messageboard where post_id = ?");

if($st == false){
	echo $db->errno . ' ' . $db->error;
} else{
	$st->bind_param('s', $_GET['thread']);
	$st->execute();
	$res = $st->get_result();
}

foreach($res as $row){
	echo '<hr><div class="thread">';
	echo '<div class="phead">';
	echo '<h3 class="subjectline">' . $row['subject'] . '</h3>';
	echo '<p class="meta">';
	echo '<inline class="name">' . $row['name'] . '</inline> ';
	echo '<inline class="tripcode" style="background-color: #' . tripcolor($row['tripcode']) . ';">' . $row['tripcode'] . '</inline> | ';
	echo $row['post_date'] . ' | ';
	echo '<a href="/thread.php/?thread=' . $row['post_id'] . '"> No. ' . $row['post_id'] . '</a>';
	echo '</p>';
	echo '</div><p class="content">';
	echo $row['content'];
	echo '</p></div>';
}

$st = $db->prepare("select * from www.messageboard where is_op = 0 and reply_to = ?");

if($st == false){
	echo $db->errno . ' ' . $db->error;
} else{
	$st->bind_param('s', $_GET['thread']);
	$st->execute();
	$res = $st->get_result();
}

foreach($res as $row){
	echo '<hr><div class="thread">';
	echo '<div class="phead" id="' . $row['post_id'] . '">';
	echo '<h3 class="subjectline">' . $row['subject'] . '</h3>';
	echo '<p class="meta">';
	echo '<inline class="name">' . $row['name'] . '</inline> ';
	echo '<inline class="tripcode" style="background-color: #' . tripcolor($row['tripcode']) . ';">' . $row['tripcode'] . '</inline> | ';
	echo $row['post_date'] . ' | ';
	echo 'No. ' . $row['post_id'];
	echo '</p>';
	echo '</div><p class="content">';
	echo preg_replace('/&gt;&gt;(.*?)<br\ \/>/i', '<span class="quotelink"><a href="#$1">&gt;&gt;$1</a></span>', preg_replace('/&gt;(.*?)\n/i', '<span class="quote">&gt;$1</span>', $row['content']));
	echo '</p></div>';
}

?>


</div>
</body>
</html>
