<?php
include 'dbcon.php';
include 'tripcode.php';
// check form

if(!isset($_POST['subject']) || trim($_POST['subject']) == ''){
	die("You must include a subject");
}

if(!isset($_POST['content']) || trim($_POST['content']) == ''){
	die("You must write something");
}

// detect anon
if(!isset($_POST['name']) || trim($_POST['name']) == ''){
	$name = 'Anonymous';
} else{
	$name = $_POST['name'];
}

if(!isset($_POST['tripcode']) || trim($_POST['tripcode']) == ''){
	$tripcode = NULL;
} else{
	$tripcode = gentripcode($_POST['tripcode']);
}

// add thread
$maxthreads = 20;

// get number of threads
$res = $db->query("select count(*) as total from www.messageboard where is_op = 1");
$refinedres = mysqli_fetch_assoc($res);
$nthreads = $refinedres['total'];

// kill thread and it's replies
if($nthreads >= $maxthreads){
	$res = $db->query("select * from www.messageboard where is_op = 1 order by post_date asc LIMIT 1");

	$resarr = $res->fetch_all();

	$res = $db->query("delete from www.messageboard where post_id = " . $resarr[0][0]);
	$res = $db->query("delete from www.messageboard where reply_to = " . $resarr[0][0]);
} 

// add thead

$st = $db->prepare("insert into www.messageboard(is_op, reply_to, subject, name, content, post_date, tripcode, user_ip) values(1, NULL, ?, ?, ?, current_timestamp(), ?, ?)");

if($st == false){
	echo $db->errno . ' ' . $db->error;
} else{
	$st->bind_param('sssss', htmlentities($_POST['subject']), htmlentities($name), nl2br(htmlentities($_POST['content'])), htmlentities($tripcode), $user_ip);
	$st->execute();
}
header('location: index.php');

?>
