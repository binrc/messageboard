# Installation
## 1. prerequisites 

Install a basic \*AMP stack. Specifically, you need mysql or mariadb. 

## 2. database

Open `bootstrap.sql` in a text editor. Find the line that looks like this and replace `y3FvW1u3B3U` with your password of choice. Make sure to keep the quotes around your new password

```sql
create user if not exists www@localhost identified by 'y3FvW1u3B3U' ;
```

Now, run something like below. This will differ if your database doesn't support socket auth.

```bash
mysql < bootstrap.sql
```

Now, you need to change the password in `dbcon.php`. Find the line that looks like this and change the password in the quotes to your own:

```php
$pass = "y3FvW1u3B3U";
```


## 3. change other passwords, hashes, etc

### deletion password

In order to secure your installation, you might want to change the password in `dodelete.php`. This password is stored as a sha512sum, you should generate your own. 

```php
//hash("sha512", "Yes, do as I say!");
$passck = "7558c5d21651c2fd42815e2ac6061daf487f9d6be3d861a74992e20774fb853a3fbcb387112d315cbc69fd9c703925fbc01de562009ce55cd77d26ab6d2cdb71";
```

### tripcode hashes

Changing your tripcode hashes will help prevent users from easily cracking the tripcode. Change the values in the quotes:

```php
$salt0 = "1xRdJWFJlEM";
$salt1 = "SHEMURbDZsz";
$salt2 = "W7zmtcfKYby";
$salt3 = "B45KxT9JgYo";
```

# Clean up after installation

either remove `bootstrap.sql` or instruct apache not to serve it. Removing it will be easier

# paranoia mode

move all of the passwords into your `php.ini`
