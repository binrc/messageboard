create user if not exists www@localhost identified by 'y3FvW1u3B3U' ;
create database if not exists www;
create table if not exists www.messageboard (post_id int not null auto_increment primary key, is_op bool, reply_to int, subject tinytext, name tinytext, content text not null, post_date datetime, tripcode text, user_ip varchar(45));
grant all on www.messageboard to 'www'@'localhost';
insert into www.messageboard(is_op, reply_to, subject, name, content, post_date, user_ip) values(1, NULL, "test post", "testuser", "test string here", current_timestamp(), '0.0.0.0');

create table if not exists www.messageboarddeletions (deletion_id int not null auto_increment primary key, post_id int not null, reason text not null, deleter_ip varchar(45), offender_ip varchar(45), offender_content text);
grant all on www.messageboarddeletions to 'www'@'localhost';

